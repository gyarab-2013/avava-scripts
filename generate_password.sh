#!/bin/bash

username=$1
pass="$(openssl rand -base64 32)"

#Sets his/her password
echo -e "$pass\n$pass" | passwd -q $username > /dev/null 2>&1

printf $pass
exit 0
