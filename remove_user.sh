#!/bin/bash
username=$1

#Removes specified user
userdel -r $username > /dev/null 2>&1

#Exits depending on the last exit code
exit $?
