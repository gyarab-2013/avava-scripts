#!/bin/bash

#Gets all gecos
emails="$(awk -F ":" '{print $5}' /etc/passwd)"

#Generates random password
pass="$(openssl rand -base64 32)"

username=$1
email=$2

#Checks if the gecos already contain the email
if [[ $emails == *"$email"* ]]
then
	printf "invalid_email"
	exit 1
fi

#Adds the user
adduser $username --gecos $email --shell "/usr/bin/fish" --disabled-login --quiet

#If the program threw an error delete the password file and return error code
if [[ $? != "0" ]]
then
	printf "general_error"
	exit 1
fi

#Sets his/her password
echo -e "$pass\n$pass" | passwd -q $username > /dev/null 2>&1

#Removes the temporary file and exits successfully
printf $pass
exit 0
