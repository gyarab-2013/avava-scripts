#!/bin/bash

# This script should accept a single parameter 'email' and return either
# username and exit code 0, if an account has been found with that email;
# a non-zero exit code, if no account has been found with that email

email=$1

#Gets all gecos
emails="$(awk -F ':' '{print $5}' /etc/passwd)"

#Checks if the gecos already contain the email
if [[ $emails != *"$email"* ]]
then
	exit 1
fi

user="$(grep -F $email /etc/passwd)"
split=(${user//:/ })

printf $split
exit 0
