# AVAVA Scripts
Description of various scripts used for user-management.
Additional information is located in the actual script files located [here](https://gitlab.com/gyarab-2013/avava-scripts).

## register
This script adds a new user. Each user needs a unique username and email, having multiple accounts per
email is not allowed. Every user gets a temporary password after the registration.
### Arguments:
- Username
- Email
### Return value:
- Exit code -> Whether the user could be added
- In case of an error a short error description gets printed into STDOUT
- In case of a success a random generated temporary password gets printed into STDOUT
---
## remove_user
This script removes a user.
### Arguments:
- Username
### Return value:
- Exit code of userdel
---
## generate_password
This script generates a random password and assigns it to a user.
### Arguments:
- Username
### Return value:
- Exit code of passwd
---
## get_username
Gets the username of an user from his email.
### Arguments:
- Email
### Return value:
- Exit code -> Whether the user exists
- If the user exists then his/her username gets printed into STDOUT
---
## is_registered
Returns whether an user exists
### Arguments:
- Email
### Return value:
- Whether the user exists