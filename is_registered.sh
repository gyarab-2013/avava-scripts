#!/bin/bash
email=$1

#Gets all gecos
emails="$(awk -F ":" '{print $5}' /etc/passwd)"

#Checks if the gecos already contain the email
if [[ $emails == *"$email"* ]]
then
	exit 0
fi

exit 1
